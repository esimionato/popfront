package pop.service;

import pop.service.dto.PlayerVcurrencyBalanceDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing PlayerVcurrencyBalance.
 */
public interface PlayerVcurrencyBalanceService {

    /**
     * Save a playerVcurrencyBalance.
     *
     * @param playerVcurrencyBalanceDTO the entity to save
     * @return the persisted entity
     */
    PlayerVcurrencyBalanceDTO save(PlayerVcurrencyBalanceDTO playerVcurrencyBalanceDTO);

    /**
     *  Get all the playerVcurrencyBalances.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PlayerVcurrencyBalanceDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" playerVcurrencyBalance.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PlayerVcurrencyBalanceDTO findOne(Long id);

    /**
     *  Delete the "id" playerVcurrencyBalance.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
