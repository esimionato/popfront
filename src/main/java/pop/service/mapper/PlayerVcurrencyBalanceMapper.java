package pop.service.mapper;

import pop.domain.*;
import pop.service.dto.PlayerVcurrencyBalanceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity PlayerVcurrencyBalance and its DTO PlayerVcurrencyBalanceDTO.
 */
@Mapper(componentModel = "spring", uses = {PlayerMapper.class, VirtualCurrencyMapper.class})
public interface PlayerVcurrencyBalanceMapper extends EntityMapper<PlayerVcurrencyBalanceDTO, PlayerVcurrencyBalance> {

    @Mapping(source = "player.id", target = "playerId")
    @Mapping(source = "player.popId", target = "playerPopId")
    @Mapping(source = "virtual_currency.id", target = "virtual_currencyId")
    @Mapping(source = "virtual_currency.currencyCode", target = "virtual_currencyCurrencyCode")
    PlayerVcurrencyBalanceDTO toDto(PlayerVcurrencyBalance playerVcurrencyBalance); 

    @Mapping(source = "playerId", target = "player")
    @Mapping(source = "virtual_currencyId", target = "virtual_currency")
    PlayerVcurrencyBalance toEntity(PlayerVcurrencyBalanceDTO playerVcurrencyBalanceDTO);

    default PlayerVcurrencyBalance fromId(Long id) {
        if (id == null) {
            return null;
        }
        PlayerVcurrencyBalance playerVcurrencyBalance = new PlayerVcurrencyBalance();
        playerVcurrencyBalance.setId(id);
        return playerVcurrencyBalance;
    }
}
