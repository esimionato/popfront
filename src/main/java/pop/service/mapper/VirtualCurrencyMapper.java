package pop.service.mapper;

import pop.domain.*;
import pop.service.dto.VirtualCurrencyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity VirtualCurrency and its DTO VirtualCurrencyDTO.
 */
@Mapper(componentModel = "spring", uses = {GameMapper.class})
public interface VirtualCurrencyMapper extends EntityMapper<VirtualCurrencyDTO, VirtualCurrency> {

    @Mapping(source = "game.id", target = "gameId")
    @Mapping(source = "game.titleId", target = "gameTitleId")
    VirtualCurrencyDTO toDto(VirtualCurrency virtualCurrency); 

    @Mapping(source = "gameId", target = "game")
    VirtualCurrency toEntity(VirtualCurrencyDTO virtualCurrencyDTO);

    default VirtualCurrency fromId(Long id) {
        if (id == null) {
            return null;
        }
        VirtualCurrency virtualCurrency = new VirtualCurrency();
        virtualCurrency.setId(id);
        return virtualCurrency;
    }
}
