package pop.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the VirtualCurrency entity.
 */
public class VirtualCurrencyDTO implements Serializable {

    private Long id;

    private String currencyIconUrl;

    @NotNull
    @Size(max = 2)
    @Pattern(regexp = "^[_'.@A-Za-z0-9-]*$")
    private String currencyCode;

    @NotNull
    @Size(max = 80)
    @Pattern(regexp = "^[_'.@A-Za-z0-9-]*$")
    private String currencyName;

    private Integer initialDeposit;

    private Integer rechargeRate;

    private Integer maxLimit;

    private Integer currencyRatio;

    private Boolean currencyBase;

    private Long gameId;

    private String gameTitleId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCurrencyIconUrl() {
        return currencyIconUrl;
    }

    public void setCurrencyIconUrl(String currencyIconUrl) {
        this.currencyIconUrl = currencyIconUrl;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public Integer getInitialDeposit() {
        return initialDeposit;
    }

    public void setInitialDeposit(Integer initialDeposit) {
        this.initialDeposit = initialDeposit;
    }

    public Integer getRechargeRate() {
        return rechargeRate;
    }

    public void setRechargeRate(Integer rechargeRate) {
        this.rechargeRate = rechargeRate;
    }

    public Integer getMaxLimit() {
        return maxLimit;
    }

    public void setMaxLimit(Integer maxLimit) {
        this.maxLimit = maxLimit;
    }

    public Integer getCurrencyRatio() {
        return currencyRatio;
    }

    public void setCurrencyRatio(Integer currencyRatio) {
        this.currencyRatio = currencyRatio;
    }

    public Boolean isCurrencyBase() {
        return currencyBase;
    }

    public void setCurrencyBase(Boolean currencyBase) {
        this.currencyBase = currencyBase;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public String getGameTitleId() {
        return gameTitleId;
    }

    public void setGameTitleId(String gameTitleId) {
        this.gameTitleId = gameTitleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VirtualCurrencyDTO virtualCurrencyDTO = (VirtualCurrencyDTO) o;
        if(virtualCurrencyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), virtualCurrencyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "VirtualCurrencyDTO{" +
            "id=" + getId() +
            ", currencyIconUrl='" + getCurrencyIconUrl() + "'" +
            ", currencyCode='" + getCurrencyCode() + "'" +
            ", currencyName='" + getCurrencyName() + "'" +
            ", initialDeposit='" + getInitialDeposit() + "'" +
            ", rechargeRate='" + getRechargeRate() + "'" +
            ", maxLimit='" + getMaxLimit() + "'" +
            ", currencyRatio='" + getCurrencyRatio() + "'" +
            ", currencyBase='" + isCurrencyBase() + "'" +
            "}";
    }
}
