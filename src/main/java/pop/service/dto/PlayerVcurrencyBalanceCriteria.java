package pop.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the PlayerVcurrencyBalance entity. This class is used in PlayerVcurrencyBalanceResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /player-vcurrency-balances?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PlayerVcurrencyBalanceCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private IntegerFilter amount;

    private LongFilter playerId;

    private LongFilter virtual_currencyId;

    public PlayerVcurrencyBalanceCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getAmount() {
        return amount;
    }

    public void setAmount(IntegerFilter amount) {
        this.amount = amount;
    }

    public LongFilter getPlayerId() {
        return playerId;
    }

    public void setPlayerId(LongFilter playerId) {
        this.playerId = playerId;
    }

    public LongFilter getVirtual_currencyId() {
        return virtual_currencyId;
    }

    public void setVirtual_currencyId(LongFilter virtual_currencyId) {
        this.virtual_currencyId = virtual_currencyId;
    }

    @Override
    public String toString() {
        return "PlayerVcurrencyBalanceCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (amount != null ? "amount=" + amount + ", " : "") +
                (playerId != null ? "playerId=" + playerId + ", " : "") +
                (virtual_currencyId != null ? "virtual_currencyId=" + virtual_currencyId + ", " : "") +
            "}";
    }

}
