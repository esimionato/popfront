package pop.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the VirtualCurrency entity. This class is used in VirtualCurrencyResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /virtual-currencies?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class VirtualCurrencyCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter currencyIconUrl;

    private StringFilter currencyCode;

    private StringFilter currencyName;

    private IntegerFilter initialDeposit;

    private IntegerFilter rechargeRate;

    private IntegerFilter maxLimit;

    private IntegerFilter currencyRatio;

    private BooleanFilter currencyBase;

    private LongFilter gameId;

    public VirtualCurrencyCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCurrencyIconUrl() {
        return currencyIconUrl;
    }

    public void setCurrencyIconUrl(StringFilter currencyIconUrl) {
        this.currencyIconUrl = currencyIconUrl;
    }

    public StringFilter getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(StringFilter currencyCode) {
        this.currencyCode = currencyCode;
    }

    public StringFilter getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(StringFilter currencyName) {
        this.currencyName = currencyName;
    }

    public IntegerFilter getInitialDeposit() {
        return initialDeposit;
    }

    public void setInitialDeposit(IntegerFilter initialDeposit) {
        this.initialDeposit = initialDeposit;
    }

    public IntegerFilter getRechargeRate() {
        return rechargeRate;
    }

    public void setRechargeRate(IntegerFilter rechargeRate) {
        this.rechargeRate = rechargeRate;
    }

    public IntegerFilter getMaxLimit() {
        return maxLimit;
    }

    public void setMaxLimit(IntegerFilter maxLimit) {
        this.maxLimit = maxLimit;
    }

    public IntegerFilter getCurrencyRatio() {
        return currencyRatio;
    }

    public void setCurrencyRatio(IntegerFilter currencyRatio) {
        this.currencyRatio = currencyRatio;
    }

    public BooleanFilter getCurrencyBase() {
        return currencyBase;
    }

    public void setCurrencyBase(BooleanFilter currencyBase) {
        this.currencyBase = currencyBase;
    }

    public LongFilter getGameId() {
        return gameId;
    }

    public void setGameId(LongFilter gameId) {
        this.gameId = gameId;
    }

    @Override
    public String toString() {
        return "VirtualCurrencyCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (currencyIconUrl != null ? "currencyIconUrl=" + currencyIconUrl + ", " : "") +
                (currencyCode != null ? "currencyCode=" + currencyCode + ", " : "") +
                (currencyName != null ? "currencyName=" + currencyName + ", " : "") +
                (initialDeposit != null ? "initialDeposit=" + initialDeposit + ", " : "") +
                (rechargeRate != null ? "rechargeRate=" + rechargeRate + ", " : "") +
                (maxLimit != null ? "maxLimit=" + maxLimit + ", " : "") +
                (currencyRatio != null ? "currencyRatio=" + currencyRatio + ", " : "") +
                (currencyBase != null ? "currencyBase=" + currencyBase + ", " : "") +
                (gameId != null ? "gameId=" + gameId + ", " : "") +
            "}";
    }

}
