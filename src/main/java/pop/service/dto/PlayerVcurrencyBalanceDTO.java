package pop.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the PlayerVcurrencyBalance entity.
 */
public class PlayerVcurrencyBalanceDTO implements Serializable {

    private Long id;

    private Integer amount;

    private Long playerId;

    private String playerPopId;

    private Long virtual_currencyId;

    private String virtual_currencyCurrencyCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public String getPlayerPopId() {
        return playerPopId;
    }

    public void setPlayerPopId(String playerPopId) {
        this.playerPopId = playerPopId;
    }

    public Long getVirtual_currencyId() {
        return virtual_currencyId;
    }

    public void setVirtual_currencyId(Long VirtualCurrencyId) {
        this.virtual_currencyId = VirtualCurrencyId;
    }

    public String getVirtual_currencyCurrencyCode() {
        return virtual_currencyCurrencyCode;
    }

    public void setVirtual_currencyCurrencyCode(String VirtualCurrencyCurrencyCode) {
        this.virtual_currencyCurrencyCode = VirtualCurrencyCurrencyCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PlayerVcurrencyBalanceDTO playerVcurrencyBalanceDTO = (PlayerVcurrencyBalanceDTO) o;
        if(playerVcurrencyBalanceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), playerVcurrencyBalanceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlayerVcurrencyBalanceDTO{" +
            "id=" + getId() +
            ", amount='" + getAmount() + "'" +
            "}";
    }
}
