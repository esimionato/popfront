package pop.repository;

import pop.domain.PlayerVcurrencyBalance;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PlayerVcurrencyBalance entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlayerVcurrencyBalanceRepository extends JpaRepository<PlayerVcurrencyBalance, Long>, JpaSpecificationExecutor<PlayerVcurrencyBalance> {

}
