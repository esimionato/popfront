package pop.web.rest;

import com.codahale.metrics.annotation.Timed;
import pop.service.PlayerVcurrencyBalanceService;
import pop.web.rest.errors.BadRequestAlertException;
import pop.web.rest.util.HeaderUtil;
import pop.web.rest.util.PaginationUtil;
import pop.service.dto.PlayerVcurrencyBalanceDTO;
import pop.service.dto.PlayerVcurrencyBalanceCriteria;
import pop.service.PlayerVcurrencyBalanceQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PlayerVcurrencyBalance.
 */
@RestController
@RequestMapping("/api")
public class PlayerVcurrencyBalanceResource {

    private final Logger log = LoggerFactory.getLogger(PlayerVcurrencyBalanceResource.class);

    private static final String ENTITY_NAME = "playerVcurrencyBalance";

    private final PlayerVcurrencyBalanceService playerVcurrencyBalanceService;

    private final PlayerVcurrencyBalanceQueryService playerVcurrencyBalanceQueryService;

    public PlayerVcurrencyBalanceResource(PlayerVcurrencyBalanceService playerVcurrencyBalanceService, PlayerVcurrencyBalanceQueryService playerVcurrencyBalanceQueryService) {
        this.playerVcurrencyBalanceService = playerVcurrencyBalanceService;
        this.playerVcurrencyBalanceQueryService = playerVcurrencyBalanceQueryService;
    }

    /**
     * POST  /player-vcurrency-balances : Create a new playerVcurrencyBalance.
     *
     * @param playerVcurrencyBalanceDTO the playerVcurrencyBalanceDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new playerVcurrencyBalanceDTO, or with status 400 (Bad Request) if the playerVcurrencyBalance has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/player-vcurrency-balances")
    @Timed
    public ResponseEntity<PlayerVcurrencyBalanceDTO> createPlayerVcurrencyBalance(@RequestBody PlayerVcurrencyBalanceDTO playerVcurrencyBalanceDTO) throws URISyntaxException {
        log.debug("REST request to save PlayerVcurrencyBalance : {}", playerVcurrencyBalanceDTO);
        if (playerVcurrencyBalanceDTO.getId() != null) {
            throw new BadRequestAlertException("A new playerVcurrencyBalance cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlayerVcurrencyBalanceDTO result = playerVcurrencyBalanceService.save(playerVcurrencyBalanceDTO);
        return ResponseEntity.created(new URI("/api/player-vcurrency-balances/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /player-vcurrency-balances : Updates an existing playerVcurrencyBalance.
     *
     * @param playerVcurrencyBalanceDTO the playerVcurrencyBalanceDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated playerVcurrencyBalanceDTO,
     * or with status 400 (Bad Request) if the playerVcurrencyBalanceDTO is not valid,
     * or with status 500 (Internal Server Error) if the playerVcurrencyBalanceDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/player-vcurrency-balances")
    @Timed
    public ResponseEntity<PlayerVcurrencyBalanceDTO> updatePlayerVcurrencyBalance(@RequestBody PlayerVcurrencyBalanceDTO playerVcurrencyBalanceDTO) throws URISyntaxException {
        log.debug("REST request to update PlayerVcurrencyBalance : {}", playerVcurrencyBalanceDTO);
        if (playerVcurrencyBalanceDTO.getId() == null) {
            return createPlayerVcurrencyBalance(playerVcurrencyBalanceDTO);
        }
        PlayerVcurrencyBalanceDTO result = playerVcurrencyBalanceService.save(playerVcurrencyBalanceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, playerVcurrencyBalanceDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /player-vcurrency-balances : get all the playerVcurrencyBalances.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of playerVcurrencyBalances in body
     */
    @GetMapping("/player-vcurrency-balances")
    @Timed
    public ResponseEntity<List<PlayerVcurrencyBalanceDTO>> getAllPlayerVcurrencyBalances(PlayerVcurrencyBalanceCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get PlayerVcurrencyBalances by criteria: {}", criteria);
        Page<PlayerVcurrencyBalanceDTO> page = playerVcurrencyBalanceQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/player-vcurrency-balances");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /player-vcurrency-balances/:id : get the "id" playerVcurrencyBalance.
     *
     * @param id the id of the playerVcurrencyBalanceDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the playerVcurrencyBalanceDTO, or with status 404 (Not Found)
     */
    @GetMapping("/player-vcurrency-balances/{id}")
    @Timed
    public ResponseEntity<PlayerVcurrencyBalanceDTO> getPlayerVcurrencyBalance(@PathVariable Long id) {
        log.debug("REST request to get PlayerVcurrencyBalance : {}", id);
        PlayerVcurrencyBalanceDTO playerVcurrencyBalanceDTO = playerVcurrencyBalanceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(playerVcurrencyBalanceDTO));
    }

    /**
     * DELETE  /player-vcurrency-balances/:id : delete the "id" playerVcurrencyBalance.
     *
     * @param id the id of the playerVcurrencyBalanceDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/player-vcurrency-balances/{id}")
    @Timed
    public ResponseEntity<Void> deletePlayerVcurrencyBalance(@PathVariable Long id) {
        log.debug("REST request to delete PlayerVcurrencyBalance : {}", id);
        playerVcurrencyBalanceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
