import { BaseEntity } from './../../shared';

export const enum GameStatus {
    'NEW',
    'DRAFT',
    'READY',
    'ACTIVE',
    'CLOSED'
}

export class Game implements BaseEntity {
    constructor(
        public id?: number,
        public titleId?: string,
        public titleName?: string,
        public status?: GameStatus,
        public version?: string,
        public titleWebsite?: string,
        public downloadUrl?: string,
        public logoUrl?: string,
        public createdAt?: any,
        public lastModifiedAt?: any,
    ) {
    }
}
