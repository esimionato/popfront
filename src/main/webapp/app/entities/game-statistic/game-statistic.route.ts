import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { GameStatisticComponent } from './game-statistic.component';
import { GameStatisticDetailComponent } from './game-statistic-detail.component';
import { GameStatisticPopupComponent } from './game-statistic-dialog.component';
import { GameStatisticDeletePopupComponent } from './game-statistic-delete-dialog.component';

export const gameStatisticRoute: Routes = [
    {
        path: 'game-statistic',
        component: GameStatisticComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.gameStatistic.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'game-statistic/:id',
        component: GameStatisticDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.gameStatistic.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const gameStatisticPopupRoute: Routes = [
    {
        path: 'game-statistic-new',
        component: GameStatisticPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.gameStatistic.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'game-statistic/:id/edit',
        component: GameStatisticPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.gameStatistic.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'game-statistic/:id/delete',
        component: GameStatisticDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.gameStatistic.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
