import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { GameStatistic } from './game-statistic.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class GameStatisticService {

    private resourceUrl = SERVER_API_URL + 'api/game-statistics';

    constructor(private http: Http) { }

    create(gameStatistic: GameStatistic): Observable<GameStatistic> {
        const copy = this.convert(gameStatistic);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(gameStatistic: GameStatistic): Observable<GameStatistic> {
        const copy = this.convert(gameStatistic);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<GameStatistic> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to GameStatistic.
     */
    private convertItemFromServer(json: any): GameStatistic {
        const entity: GameStatistic = Object.assign(new GameStatistic(), json);
        return entity;
    }

    /**
     * Convert a GameStatistic to a JSON which can be sent to the server.
     */
    private convert(gameStatistic: GameStatistic): GameStatistic {
        const copy: GameStatistic = Object.assign({}, gameStatistic);
        return copy;
    }
}
