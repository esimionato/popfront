import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PopSharedModule } from '../../shared';
import {
    GameStatisticService,
    GameStatisticPopupService,
    GameStatisticComponent,
    GameStatisticDetailComponent,
    GameStatisticDialogComponent,
    GameStatisticPopupComponent,
    GameStatisticDeletePopupComponent,
    GameStatisticDeleteDialogComponent,
    gameStatisticRoute,
    gameStatisticPopupRoute,
} from './';

const ENTITY_STATES = [
    ...gameStatisticRoute,
    ...gameStatisticPopupRoute,
];

@NgModule({
    imports: [
        PopSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        GameStatisticComponent,
        GameStatisticDetailComponent,
        GameStatisticDialogComponent,
        GameStatisticDeleteDialogComponent,
        GameStatisticPopupComponent,
        GameStatisticDeletePopupComponent,
    ],
    entryComponents: [
        GameStatisticComponent,
        GameStatisticDialogComponent,
        GameStatisticPopupComponent,
        GameStatisticDeleteDialogComponent,
        GameStatisticDeletePopupComponent,
    ],
    providers: [
        GameStatisticService,
        GameStatisticPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PopGameStatisticModule {}
