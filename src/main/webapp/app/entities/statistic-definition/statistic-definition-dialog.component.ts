import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { StatisticDefinition } from './statistic-definition.model';
import { StatisticDefinitionPopupService } from './statistic-definition-popup.service';
import { StatisticDefinitionService } from './statistic-definition.service';
import { Game, GameService } from '../game';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-statistic-definition-dialog',
    templateUrl: './statistic-definition-dialog.component.html'
})
export class StatisticDefinitionDialogComponent implements OnInit {

    statisticDefinition: StatisticDefinition;
    isSaving: boolean;

    games: Game[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private statisticDefinitionService: StatisticDefinitionService,
        private gameService: GameService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.gameService.query()
            .subscribe((res: ResponseWrapper) => { this.games = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.statisticDefinition.id !== undefined) {
            this.subscribeToSaveResponse(
                this.statisticDefinitionService.update(this.statisticDefinition));
        } else {
            this.subscribeToSaveResponse(
                this.statisticDefinitionService.create(this.statisticDefinition));
        }
    }

    private subscribeToSaveResponse(result: Observable<StatisticDefinition>) {
        result.subscribe((res: StatisticDefinition) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: StatisticDefinition) {
        this.eventManager.broadcast({ name: 'statisticDefinitionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackGameById(index: number, item: Game) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-statistic-definition-popup',
    template: ''
})
export class StatisticDefinitionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private statisticDefinitionPopupService: StatisticDefinitionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.statisticDefinitionPopupService
                    .open(StatisticDefinitionDialogComponent as Component, params['id']);
            } else {
                this.statisticDefinitionPopupService
                    .open(StatisticDefinitionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
