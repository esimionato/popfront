import { BaseEntity } from './../../shared';

export const enum StatisticCategory {
    'GAMEPLAY',
    'PROGRESION',
    'ECONOMY',
    'PERFORMANCE',
    'LEVELING'
}

export const enum AggregationMethod {
    'LAST',
    'MIN',
    'MAX',
    'SUM',
    'COUNT'
}

export const enum ResetFrequency {
    'MANUALLY',
    'HOURLY',
    'DAILY',
    'WEEKLY',
    'MONTHLY'
}

export class StatisticDefinition implements BaseEntity {
    constructor(
        public id?: number,
        public stCode?: string,
        public stName?: string,
        public stDescription?: string,
        public stMinValue?: number,
        public stMaxValue?: number,
        public stInitialValue?: number,
        public stVariable?: string,
        public stCategory?: StatisticCategory,
        public aggregationMethod?: AggregationMethod,
        public resetFrequency?: ResetFrequency,
        public gameId?: number,
    ) {
    }
}
