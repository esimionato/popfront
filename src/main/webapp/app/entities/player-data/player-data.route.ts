import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PlayerDataComponent } from './player-data.component';
import { PlayerDataDetailComponent } from './player-data-detail.component';
import { PlayerDataPopupComponent } from './player-data-dialog.component';
import { PlayerDataDeletePopupComponent } from './player-data-delete-dialog.component';

export const playerDataRoute: Routes = [
    {
        path: 'player-data',
        component: PlayerDataComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.playerData.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'player-data/:id',
        component: PlayerDataDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.playerData.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const playerDataPopupRoute: Routes = [
    {
        path: 'player-data-new',
        component: PlayerDataPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.playerData.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'player-data/:id/edit',
        component: PlayerDataPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.playerData.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'player-data/:id/delete',
        component: PlayerDataDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.playerData.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
