import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { PlayerData } from './player-data.model';
import { PlayerDataService } from './player-data.service';

@Component({
    selector: 'jhi-player-data-detail',
    templateUrl: './player-data-detail.component.html'
})
export class PlayerDataDetailComponent implements OnInit, OnDestroy {

    playerData: PlayerData;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private playerDataService: PlayerDataService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPlayerData();
    }

    load(id) {
        this.playerDataService.find(id).subscribe((playerData) => {
            this.playerData = playerData;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPlayerData() {
        this.eventSubscriber = this.eventManager.subscribe(
            'playerDataListModification',
            (response) => this.load(this.playerData.id)
        );
    }
}
