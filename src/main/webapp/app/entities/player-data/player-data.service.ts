import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { PlayerData } from './player-data.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PlayerDataService {

    private resourceUrl = SERVER_API_URL + 'api/player-data';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(playerData: PlayerData): Observable<PlayerData> {
        const copy = this.convert(playerData);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(playerData: PlayerData): Observable<PlayerData> {
        const copy = this.convert(playerData);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<PlayerData> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to PlayerData.
     */
    private convertItemFromServer(json: any): PlayerData {
        const entity: PlayerData = Object.assign(new PlayerData(), json);
        entity.lastModifiedAt = this.dateUtils
            .convertDateTimeFromServer(json.lastModifiedAt);
        entity.createdAt = this.dateUtils
            .convertDateTimeFromServer(json.createdAt);
        return entity;
    }

    /**
     * Convert a PlayerData to a JSON which can be sent to the server.
     */
    private convert(playerData: PlayerData): PlayerData {
        const copy: PlayerData = Object.assign({}, playerData);

        copy.lastModifiedAt = this.dateUtils.toDate(playerData.lastModifiedAt);

        copy.createdAt = this.dateUtils.toDate(playerData.createdAt);
        return copy;
    }
}
