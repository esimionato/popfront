import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { PlayerData } from './player-data.model';
import { PlayerDataService } from './player-data.service';

@Injectable()
export class PlayerDataPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private playerDataService: PlayerDataService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.playerDataService.find(id).subscribe((playerData) => {
                    playerData.lastModifiedAt = this.datePipe
                        .transform(playerData.lastModifiedAt, 'yyyy-MM-ddTHH:mm:ss');
                    playerData.createdAt = this.datePipe
                        .transform(playerData.createdAt, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.playerDataModalRef(component, playerData);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.playerDataModalRef(component, new PlayerData());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    playerDataModalRef(component: Component, playerData: PlayerData): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.playerData = playerData;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
