import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PlayerData } from './player-data.model';
import { PlayerDataPopupService } from './player-data-popup.service';
import { PlayerDataService } from './player-data.service';
import { Player, PlayerService } from '../player';
import { DataDefinition, DataDefinitionService } from '../data-definition';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-player-data-dialog',
    templateUrl: './player-data-dialog.component.html'
})
export class PlayerDataDialogComponent implements OnInit {

    playerData: PlayerData;
    isSaving: boolean;

    players: Player[];

    datadefinitions: DataDefinition[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private playerDataService: PlayerDataService,
        private playerService: PlayerService,
        private dataDefinitionService: DataDefinitionService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.playerService.query()
            .subscribe((res: ResponseWrapper) => { this.players = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.dataDefinitionService.query()
            .subscribe((res: ResponseWrapper) => { this.datadefinitions = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.playerData.id !== undefined) {
            this.subscribeToSaveResponse(
                this.playerDataService.update(this.playerData));
        } else {
            this.subscribeToSaveResponse(
                this.playerDataService.create(this.playerData));
        }
    }

    private subscribeToSaveResponse(result: Observable<PlayerData>) {
        result.subscribe((res: PlayerData) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: PlayerData) {
        this.eventManager.broadcast({ name: 'playerDataListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackPlayerById(index: number, item: Player) {
        return item.id;
    }

    trackDataDefinitionById(index: number, item: DataDefinition) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-player-data-popup',
    template: ''
})
export class PlayerDataPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private playerDataPopupService: PlayerDataPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.playerDataPopupService
                    .open(PlayerDataDialogComponent as Component, params['id']);
            } else {
                this.playerDataPopupService
                    .open(PlayerDataDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
