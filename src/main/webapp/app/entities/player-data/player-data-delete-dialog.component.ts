import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PlayerData } from './player-data.model';
import { PlayerDataPopupService } from './player-data-popup.service';
import { PlayerDataService } from './player-data.service';

@Component({
    selector: 'jhi-player-data-delete-dialog',
    templateUrl: './player-data-delete-dialog.component.html'
})
export class PlayerDataDeleteDialogComponent {

    playerData: PlayerData;

    constructor(
        private playerDataService: PlayerDataService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.playerDataService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'playerDataListModification',
                content: 'Deleted an playerData'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-player-data-delete-popup',
    template: ''
})
export class PlayerDataDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private playerDataPopupService: PlayerDataPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.playerDataPopupService
                .open(PlayerDataDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
