import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { GlobalDataComponent } from './global-data.component';
import { GlobalDataDetailComponent } from './global-data-detail.component';
import { GlobalDataPopupComponent } from './global-data-dialog.component';
import { GlobalDataDeletePopupComponent } from './global-data-delete-dialog.component';

export const globalDataRoute: Routes = [
    {
        path: 'global-data',
        component: GlobalDataComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.globalData.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'global-data/:id',
        component: GlobalDataDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.globalData.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const globalDataPopupRoute: Routes = [
    {
        path: 'global-data-new',
        component: GlobalDataPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.globalData.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'global-data/:id/edit',
        component: GlobalDataPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.globalData.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'global-data/:id/delete',
        component: GlobalDataDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.globalData.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
