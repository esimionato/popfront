import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { GlobalData } from './global-data.model';
import { GlobalDataService } from './global-data.service';

@Component({
    selector: 'jhi-global-data-detail',
    templateUrl: './global-data-detail.component.html'
})
export class GlobalDataDetailComponent implements OnInit, OnDestroy {

    globalData: GlobalData;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private globalDataService: GlobalDataService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInGlobalData();
    }

    load(id) {
        this.globalDataService.find(id).subscribe((globalData) => {
            this.globalData = globalData;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInGlobalData() {
        this.eventSubscriber = this.eventManager.subscribe(
            'globalDataListModification',
            (response) => this.load(this.globalData.id)
        );
    }
}
