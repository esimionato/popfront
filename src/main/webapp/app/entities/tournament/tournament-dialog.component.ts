import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Tournament } from './tournament.model';
import { TournamentPopupService } from './tournament-popup.service';
import { TournamentService } from './tournament.service';
import { Game, GameService } from '../game';
import { XpLevelDefinition, XpLevelDefinitionService } from '../xp-level-definition';
import { EventDefinition, EventDefinitionService } from '../event-definition';
import { StatisticDefinition, StatisticDefinitionService } from '../statistic-definition';
import { VirtualCurrency, VirtualCurrencyService } from '../virtual-currency';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-tournament-dialog',
    templateUrl: './tournament-dialog.component.html'
})
export class TournamentDialogComponent implements OnInit {

    tournament: Tournament;
    isSaving: boolean;

    games: Game[];

    xpleveldefinitions: XpLevelDefinition[];

    eventdefinitions: EventDefinition[];

    statisticdefinitions: StatisticDefinition[];

    virtualcurrencies: VirtualCurrency[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private tournamentService: TournamentService,
        private gameService: GameService,
        private xpLevelDefinitionService: XpLevelDefinitionService,
        private eventDefinitionService: EventDefinitionService,
        private statisticDefinitionService: StatisticDefinitionService,
        private VirtualCurrencyService: VirtualCurrencyService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.gameService.query()
            .subscribe((res: ResponseWrapper) => { this.games = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.xpLevelDefinitionService.query()
            .subscribe((res: ResponseWrapper) => { this.xpleveldefinitions = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.eventDefinitionService.query()
            .subscribe((res: ResponseWrapper) => { this.eventdefinitions = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.statisticDefinitionService.query()
            .subscribe((res: ResponseWrapper) => { this.statisticdefinitions = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.VirtualCurrencyService.query()
            .subscribe((res: ResponseWrapper) => { this.virtualcurrencies = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.tournament.id !== undefined) {
            this.subscribeToSaveResponse(
                this.tournamentService.update(this.tournament));
        } else {
            this.subscribeToSaveResponse(
                this.tournamentService.create(this.tournament));
        }
    }

    private subscribeToSaveResponse(result: Observable<Tournament>) {
        result.subscribe((res: Tournament) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Tournament) {
        this.eventManager.broadcast({ name: 'tournamentListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackGameById(index: number, item: Game) {
        return item.id;
    }

    trackXpLevelDefinitionById(index: number, item: XpLevelDefinition) {
        return item.id;
    }

    trackEventDefinitionById(index: number, item: EventDefinition) {
        return item.id;
    }

    trackStatisticDefinitionById(index: number, item: StatisticDefinition) {
        return item.id;
    }

    trackVirtualCurrencyById(index: number, item: VirtualCurrency) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-tournament-popup',
    template: ''
})
export class TournamentPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tournamentPopupService: TournamentPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.tournamentPopupService
                    .open(TournamentDialogComponent as Component, params['id']);
            } else {
                this.tournamentPopupService
                    .open(TournamentDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
