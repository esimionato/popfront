import { BaseEntity } from './../../shared';

export const enum TournamentType {
    'PRIVATE',
    ' LEADERBOARD'
}

export const enum TournamentJoinType {
    'FREE',
    'PAID',
    'AUTOMATIC'
}

export const enum ResetFrecuency {
    'MANUALLY',
    'HOURLY',
    'DAILY',
    'WEEKLY',
    'MONTHLY'
}

export const enum Frecuency {
    'MANUALLY',
    'HOURLY',
    'DAILY',
    'WEEKLY',
    'MONTHLY'
}

export const enum TournamentStatus {
    'DRAFT',
    'ACTIVE',
    'CLOSED'
}

export class Tournament implements BaseEntity {
    constructor(
        public id?: number,
        public tnIconUrl?: string,
        public tnCode?: string,
        public tnTitle?: string,
        public tnType?: TournamentType,
        public joinType?: TournamentJoinType,
        public reseteable?: boolean,
        public resetFrecuency?: ResetFrecuency,
        public refreshFrecuency?: Frecuency,
        public notificationFrecuency?: Frecuency,
        public stCode?: string,
        public lvCode?: string,
        public limitValue?: number,
        public feeCurrencyCode?: string,
        public feeCurrencyValue?: string,
        public startedAt?: any,
        public iteration?: number,
        public duration?: number,
        public status?: TournamentStatus,
        public gameId?: number,
        public reqXpLevelId?: number,
        public goalEventId?: number,
        public goalStatisticId?: number,
        public feeCurrencyId?: number,
    ) {
        this.reseteable = false;
    }
}
