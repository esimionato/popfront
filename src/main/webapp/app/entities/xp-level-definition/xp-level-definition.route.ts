import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { XpLevelDefinitionComponent } from './xp-level-definition.component';
import { XpLevelDefinitionDetailComponent } from './xp-level-definition-detail.component';
import { XpLevelDefinitionPopupComponent } from './xp-level-definition-dialog.component';
import { XpLevelDefinitionDeletePopupComponent } from './xp-level-definition-delete-dialog.component';

export const xpLevelDefinitionRoute: Routes = [
    {
        path: 'xp-level-definition',
        component: XpLevelDefinitionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.xpLevelDefinition.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'xp-level-definition/:id',
        component: XpLevelDefinitionDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.xpLevelDefinition.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const xpLevelDefinitionPopupRoute: Routes = [
    {
        path: 'xp-level-definition-new',
        component: XpLevelDefinitionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.xpLevelDefinition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'xp-level-definition/:id/edit',
        component: XpLevelDefinitionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.xpLevelDefinition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'xp-level-definition/:id/delete',
        component: XpLevelDefinitionDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.xpLevelDefinition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
