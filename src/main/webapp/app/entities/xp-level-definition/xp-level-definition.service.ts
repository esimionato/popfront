import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { XpLevelDefinition } from './xp-level-definition.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class XpLevelDefinitionService {

    private resourceUrl = SERVER_API_URL + 'api/xp-level-definitions';

    constructor(private http: Http) { }

    create(xpLevelDefinition: XpLevelDefinition): Observable<XpLevelDefinition> {
        const copy = this.convert(xpLevelDefinition);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(xpLevelDefinition: XpLevelDefinition): Observable<XpLevelDefinition> {
        const copy = this.convert(xpLevelDefinition);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<XpLevelDefinition> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to XpLevelDefinition.
     */
    private convertItemFromServer(json: any): XpLevelDefinition {
        const entity: XpLevelDefinition = Object.assign(new XpLevelDefinition(), json);
        return entity;
    }

    /**
     * Convert a XpLevelDefinition to a JSON which can be sent to the server.
     */
    private convert(xpLevelDefinition: XpLevelDefinition): XpLevelDefinition {
        const copy: XpLevelDefinition = Object.assign({}, xpLevelDefinition);
        return copy;
    }
}
