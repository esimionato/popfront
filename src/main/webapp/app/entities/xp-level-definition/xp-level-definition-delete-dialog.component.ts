import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { XpLevelDefinition } from './xp-level-definition.model';
import { XpLevelDefinitionPopupService } from './xp-level-definition-popup.service';
import { XpLevelDefinitionService } from './xp-level-definition.service';

@Component({
    selector: 'jhi-xp-level-definition-delete-dialog',
    templateUrl: './xp-level-definition-delete-dialog.component.html'
})
export class XpLevelDefinitionDeleteDialogComponent {

    xpLevelDefinition: XpLevelDefinition;

    constructor(
        private xpLevelDefinitionService: XpLevelDefinitionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.xpLevelDefinitionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'xpLevelDefinitionListModification',
                content: 'Deleted an xpLevelDefinition'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-xp-level-definition-delete-popup',
    template: ''
})
export class XpLevelDefinitionDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private xpLevelDefinitionPopupService: XpLevelDefinitionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.xpLevelDefinitionPopupService
                .open(XpLevelDefinitionDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
