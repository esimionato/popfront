import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { XpLevelDefinition } from './xp-level-definition.model';
import { XpLevelDefinitionService } from './xp-level-definition.service';

@Component({
    selector: 'jhi-xp-level-definition-detail',
    templateUrl: './xp-level-definition-detail.component.html'
})
export class XpLevelDefinitionDetailComponent implements OnInit, OnDestroy {

    xpLevelDefinition: XpLevelDefinition;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private xpLevelDefinitionService: XpLevelDefinitionService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInXpLevelDefinitions();
    }

    load(id) {
        this.xpLevelDefinitionService.find(id).subscribe((xpLevelDefinition) => {
            this.xpLevelDefinition = xpLevelDefinition;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInXpLevelDefinitions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'xpLevelDefinitionListModification',
            (response) => this.load(this.xpLevelDefinition.id)
        );
    }
}
