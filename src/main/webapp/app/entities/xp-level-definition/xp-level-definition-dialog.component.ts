import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { XpLevelDefinition } from './xp-level-definition.model';
import { XpLevelDefinitionPopupService } from './xp-level-definition-popup.service';
import { XpLevelDefinitionService } from './xp-level-definition.service';
import { Game, GameService } from '../game';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-xp-level-definition-dialog',
    templateUrl: './xp-level-definition-dialog.component.html'
})
export class XpLevelDefinitionDialogComponent implements OnInit {

    xpLevelDefinition: XpLevelDefinition;
    isSaving: boolean;

    games: Game[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private xpLevelDefinitionService: XpLevelDefinitionService,
        private gameService: GameService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.gameService.query()
            .subscribe((res: ResponseWrapper) => { this.games = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.xpLevelDefinition.id !== undefined) {
            this.subscribeToSaveResponse(
                this.xpLevelDefinitionService.update(this.xpLevelDefinition));
        } else {
            this.subscribeToSaveResponse(
                this.xpLevelDefinitionService.create(this.xpLevelDefinition));
        }
    }

    private subscribeToSaveResponse(result: Observable<XpLevelDefinition>) {
        result.subscribe((res: XpLevelDefinition) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: XpLevelDefinition) {
        this.eventManager.broadcast({ name: 'xpLevelDefinitionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackGameById(index: number, item: Game) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-xp-level-definition-popup',
    template: ''
})
export class XpLevelDefinitionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private xpLevelDefinitionPopupService: XpLevelDefinitionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.xpLevelDefinitionPopupService
                    .open(XpLevelDefinitionDialogComponent as Component, params['id']);
            } else {
                this.xpLevelDefinitionPopupService
                    .open(XpLevelDefinitionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
