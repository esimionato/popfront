import { BaseEntity } from './../../shared';

export class XpLevelDefinition implements BaseEntity {
    constructor(
        public id?: number,
        public lvIconUrl?: string,
        public lvCode?: string,
        public lvNumber?: number,
        public xpBase?: number,
        public lvDescription?: string,
        public bonusData?: string,
        public increaseData?: string,
        public neededData?: string,
        public contentUnlocked?: string,
        public gameModeUnlocked?: string,
        public gameId?: number,
    ) {
    }
}
