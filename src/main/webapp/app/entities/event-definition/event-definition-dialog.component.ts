import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { EventDefinition } from './event-definition.model';
import { EventDefinitionPopupService } from './event-definition-popup.service';
import { EventDefinitionService } from './event-definition.service';
import { Game, GameService } from '../game';
import { StatisticDefinition, StatisticDefinitionService } from '../statistic-definition';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-event-definition-dialog',
    templateUrl: './event-definition-dialog.component.html'
})
export class EventDefinitionDialogComponent implements OnInit {

    eventDefinition: EventDefinition;
    isSaving: boolean;

    games: Game[];

    statisticdefinitions: StatisticDefinition[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private eventDefinitionService: EventDefinitionService,
        private gameService: GameService,
        private statisticDefinitionService: StatisticDefinitionService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.gameService.query()
            .subscribe((res: ResponseWrapper) => { this.games = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.statisticDefinitionService.query()
            .subscribe((res: ResponseWrapper) => { this.statisticdefinitions = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.eventDefinition.id !== undefined) {
            this.subscribeToSaveResponse(
                this.eventDefinitionService.update(this.eventDefinition));
        } else {
            this.subscribeToSaveResponse(
                this.eventDefinitionService.create(this.eventDefinition));
        }
    }

    private subscribeToSaveResponse(result: Observable<EventDefinition>) {
        result.subscribe((res: EventDefinition) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: EventDefinition) {
        this.eventManager.broadcast({ name: 'eventDefinitionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackGameById(index: number, item: Game) {
        return item.id;
    }

    trackStatisticDefinitionById(index: number, item: StatisticDefinition) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-event-definition-popup',
    template: ''
})
export class EventDefinitionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private eventDefinitionPopupService: EventDefinitionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.eventDefinitionPopupService
                    .open(EventDefinitionDialogComponent as Component, params['id']);
            } else {
                this.eventDefinitionPopupService
                    .open(EventDefinitionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
