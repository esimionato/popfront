import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { EventDefinition } from './event-definition.model';
import { EventDefinitionService } from './event-definition.service';

@Component({
    selector: 'jhi-event-definition-detail',
    templateUrl: './event-definition-detail.component.html'
})
export class EventDefinitionDetailComponent implements OnInit, OnDestroy {

    eventDefinition: EventDefinition;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private eventDefinitionService: EventDefinitionService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInEventDefinitions();
    }

    load(id) {
        this.eventDefinitionService.find(id).subscribe((eventDefinition) => {
            this.eventDefinition = eventDefinition;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInEventDefinitions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'eventDefinitionListModification',
            (response) => this.load(this.eventDefinition.id)
        );
    }
}
