import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { VirtualCurrency } from './virtual-currency.model';
import { VirtualCurrencyPopupService } from './virtual-currency-popup.service';
import { VirtualCurrencyService } from './virtual-currency.service';

@Component({
    selector: 'jhi-virtual-currency-delete-dialog',
    templateUrl: './virtual-currency-delete-dialog.component.html'
})
export class VirtualCurrencyDeleteDialogComponent {

    virtualCurrency: VirtualCurrency;

    constructor(
        private virtualCurrencyService: VirtualCurrencyService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.virtualCurrencyService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'virtualCurrencyListModification',
                content: 'Deleted an virtualCurrency'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-virtual-currency-delete-popup',
    template: ''
})
export class VirtualCurrencyDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private virtualCurrencyPopupService: VirtualCurrencyPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.virtualCurrencyPopupService
                .open(VirtualCurrencyDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
