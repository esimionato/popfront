import { BaseEntity } from './../../shared';

export class VirtualCurrency implements BaseEntity {
    constructor(
        public id?: number,
        public currencyIconUrl?: string,
        public currencyCode?: string,
        public currencyName?: string,
        public initialDeposit?: number,
        public rechargeRate?: number,
        public maxLimit?: number,
        public currencyRatio?: number,
        public currencyBase?: boolean,
        public gameId?: number,
    ) {
        this.currencyBase = false;
    }
}
