import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { VirtualCurrency } from './virtual-currency.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class VirtualCurrencyService {

    private resourceUrl = SERVER_API_URL + 'api/virtual-currencies';

    constructor(private http: Http) { }

    create(virtualCurrency: VirtualCurrency): Observable<VirtualCurrency> {
        const copy = this.convert(virtualCurrency);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(virtualCurrency: VirtualCurrency): Observable<VirtualCurrency> {
        const copy = this.convert(virtualCurrency);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<VirtualCurrency> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to VirtualCurrency.
     */
    private convertItemFromServer(json: any): VirtualCurrency {
        const entity: VirtualCurrency = Object.assign(new VirtualCurrency(), json);
        return entity;
    }

    /**
     * Convert a VirtualCurrency to a JSON which can be sent to the server.
     */
    private convert(virtualCurrency: VirtualCurrency): VirtualCurrency {
        const copy: VirtualCurrency = Object.assign({}, virtualCurrency);
        return copy;
    }
}
