import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { VirtualCurrency } from './virtual-currency.model';
import { VirtualCurrencyService } from './virtual-currency.service';

@Component({
    selector: 'jhi-virtual-currency-detail',
    templateUrl: './virtual-currency-detail.component.html'
})
export class VirtualCurrencyDetailComponent implements OnInit, OnDestroy {

    virtualCurrency: VirtualCurrency;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private virtualCurrencyService: VirtualCurrencyService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInVirtualCurrencies();
    }

    load(id) {
        this.virtualCurrencyService.find(id).subscribe((virtualCurrency) => {
            this.virtualCurrency = virtualCurrency;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInVirtualCurrencies() {
        this.eventSubscriber = this.eventManager.subscribe(
            'virtualCurrencyListModification',
            (response) => this.load(this.virtualCurrency.id)
        );
    }
}
