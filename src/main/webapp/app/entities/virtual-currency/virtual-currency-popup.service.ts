import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { VirtualCurrency } from './virtual-currency.model';
import { VirtualCurrencyService } from './virtual-currency.service';

@Injectable()
export class VirtualCurrencyPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private virtualCurrencyService: VirtualCurrencyService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.virtualCurrencyService.find(id).subscribe((virtualCurrency) => {
                    this.ngbModalRef = this.virtualCurrencyModalRef(component, virtualCurrency);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.virtualCurrencyModalRef(component, new VirtualCurrency());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    virtualCurrencyModalRef(component: Component, virtualCurrency: VirtualCurrency): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.virtualCurrency = virtualCurrency;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
