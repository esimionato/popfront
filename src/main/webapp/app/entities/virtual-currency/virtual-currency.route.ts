import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { VirtualCurrencyComponent } from './virtual-currency.component';
import { VirtualCurrencyDetailComponent } from './virtual-currency-detail.component';
import { VirtualCurrencyPopupComponent } from './virtual-currency-dialog.component';
import { VirtualCurrencyDeletePopupComponent } from './virtual-currency-delete-dialog.component';

export const virtualCurrencyRoute: Routes = [
    {
        path: 'virtual-currency',
        component: VirtualCurrencyComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.virtualCurrency.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'virtual-currency/:id',
        component: VirtualCurrencyDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.virtualCurrency.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const virtualCurrencyPopupRoute: Routes = [
    {
        path: 'virtual-currency-new',
        component: VirtualCurrencyPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.virtualCurrency.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'virtual-currency/:id/edit',
        component: VirtualCurrencyPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.virtualCurrency.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'virtual-currency/:id/delete',
        component: VirtualCurrencyDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.virtualCurrency.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
