import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DataDefinition } from './data-definition.model';
import { DataDefinitionPopupService } from './data-definition-popup.service';
import { DataDefinitionService } from './data-definition.service';

@Component({
    selector: 'jhi-data-definition-delete-dialog',
    templateUrl: './data-definition-delete-dialog.component.html'
})
export class DataDefinitionDeleteDialogComponent {

    dataDefinition: DataDefinition;

    constructor(
        private dataDefinitionService: DataDefinitionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dataDefinitionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'dataDefinitionListModification',
                content: 'Deleted an dataDefinition'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-data-definition-delete-popup',
    template: ''
})
export class DataDefinitionDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dataDefinitionPopupService: DataDefinitionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.dataDefinitionPopupService
                .open(DataDefinitionDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
