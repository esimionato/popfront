import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { DataDefinition } from './data-definition.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class DataDefinitionService {

    private resourceUrl = SERVER_API_URL + 'api/data-definitions';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(dataDefinition: DataDefinition): Observable<DataDefinition> {
        const copy = this.convert(dataDefinition);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(dataDefinition: DataDefinition): Observable<DataDefinition> {
        const copy = this.convert(dataDefinition);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<DataDefinition> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to DataDefinition.
     */
    private convertItemFromServer(json: any): DataDefinition {
        const entity: DataDefinition = Object.assign(new DataDefinition(), json);
        entity.createdAt = this.dateUtils
            .convertDateTimeFromServer(json.createdAt);
        entity.lastModifiedAt = this.dateUtils
            .convertDateTimeFromServer(json.lastModifiedAt);
        return entity;
    }

    /**
     * Convert a DataDefinition to a JSON which can be sent to the server.
     */
    private convert(dataDefinition: DataDefinition): DataDefinition {
        const copy: DataDefinition = Object.assign({}, dataDefinition);

        copy.createdAt = this.dateUtils.toDate(dataDefinition.createdAt);

        copy.lastModifiedAt = this.dateUtils.toDate(dataDefinition.lastModifiedAt);
        return copy;
    }
}
