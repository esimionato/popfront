import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PopSharedModule } from '../../shared';
import {
    DataDefinitionService,
    DataDefinitionPopupService,
    DataDefinitionComponent,
    DataDefinitionDetailComponent,
    DataDefinitionDialogComponent,
    DataDefinitionPopupComponent,
    DataDefinitionDeletePopupComponent,
    DataDefinitionDeleteDialogComponent,
    dataDefinitionRoute,
    dataDefinitionPopupRoute,
} from './';

const ENTITY_STATES = [
    ...dataDefinitionRoute,
    ...dataDefinitionPopupRoute,
];

@NgModule({
    imports: [
        PopSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        DataDefinitionComponent,
        DataDefinitionDetailComponent,
        DataDefinitionDialogComponent,
        DataDefinitionDeleteDialogComponent,
        DataDefinitionPopupComponent,
        DataDefinitionDeletePopupComponent,
    ],
    entryComponents: [
        DataDefinitionComponent,
        DataDefinitionDialogComponent,
        DataDefinitionPopupComponent,
        DataDefinitionDeleteDialogComponent,
        DataDefinitionDeletePopupComponent,
    ],
    providers: [
        DataDefinitionService,
        DataDefinitionPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PopDataDefinitionModule {}
