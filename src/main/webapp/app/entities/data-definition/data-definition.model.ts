import { BaseEntity } from './../../shared';

export const enum DataScope {
    'PUBLIC',
    'PRIVATE',
    'INTERNAL'
}

export const enum DataPermission {
    'WRITE',
    'ONLY_READ'
}

export class DataDefinition implements BaseEntity {
    constructor(
        public id?: number,
        public dataKey?: string,
        public defaultValue?: string,
        public scope?: DataScope,
        public permissions?: DataPermission,
        public dataVersion?: string,
        public createdAt?: any,
        public lastModifiedAt?: any,
        public gameId?: number,
    ) {
    }
}
