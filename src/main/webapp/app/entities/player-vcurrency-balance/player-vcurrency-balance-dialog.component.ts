import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PlayerVcurrencyBalance } from './player-vcurrency-balance.model';
import { PlayerVcurrencyBalancePopupService } from './player-vcurrency-balance-popup.service';
import { PlayerVcurrencyBalanceService } from './player-vcurrency-balance.service';
import { Player, PlayerService } from '../player';
import { VirtualCurrency, VirtualCurrencyService } from '../virtual-currency';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-player-vcurrency-balance-dialog',
    templateUrl: './player-vcurrency-balance-dialog.component.html'
})
export class PlayerVcurrencyBalanceDialogComponent implements OnInit {

    playerVcurrencyBalance: PlayerVcurrencyBalance;
    isSaving: boolean;

    players: Player[];

    virtualcurrencies: VirtualCurrency[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private playerVcurrencyBalanceService: PlayerVcurrencyBalanceService,
        private playerService: PlayerService,
        private virtualCurrencyService: VirtualCurrencyService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.playerService.query()
            .subscribe((res: ResponseWrapper) => { this.players = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.virtualCurrencyService.query()
            .subscribe((res: ResponseWrapper) => { this.virtualcurrencies = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.playerVcurrencyBalance.id !== undefined) {
            this.subscribeToSaveResponse(
                this.playerVcurrencyBalanceService.update(this.playerVcurrencyBalance));
        } else {
            this.subscribeToSaveResponse(
                this.playerVcurrencyBalanceService.create(this.playerVcurrencyBalance));
        }
    }

    private subscribeToSaveResponse(result: Observable<PlayerVcurrencyBalance>) {
        result.subscribe((res: PlayerVcurrencyBalance) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: PlayerVcurrencyBalance) {
        this.eventManager.broadcast({ name: 'playerVcurrencyBalanceListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackPlayerById(index: number, item: Player) {
        return item.id;
    }

    trackVirtualCurrencyById(index: number, item: VirtualCurrency) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-player-vcurrency-balance-popup',
    template: ''
})
export class PlayerVcurrencyBalancePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private playerVcurrencyBalancePopupService: PlayerVcurrencyBalancePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.playerVcurrencyBalancePopupService
                    .open(PlayerVcurrencyBalanceDialogComponent as Component, params['id']);
            } else {
                this.playerVcurrencyBalancePopupService
                    .open(PlayerVcurrencyBalanceDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
