import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { PlayerVcurrencyBalance } from './player-vcurrency-balance.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PlayerVcurrencyBalanceService {

    private resourceUrl = SERVER_API_URL + 'api/player-vcurrency-balances';

    constructor(private http: Http) { }

    create(playerVcurrencyBalance: PlayerVcurrencyBalance): Observable<PlayerVcurrencyBalance> {
        const copy = this.convert(playerVcurrencyBalance);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(playerVcurrencyBalance: PlayerVcurrencyBalance): Observable<PlayerVcurrencyBalance> {
        const copy = this.convert(playerVcurrencyBalance);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<PlayerVcurrencyBalance> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to PlayerVcurrencyBalance.
     */
    private convertItemFromServer(json: any): PlayerVcurrencyBalance {
        const entity: PlayerVcurrencyBalance = Object.assign(new PlayerVcurrencyBalance(), json);
        return entity;
    }

    /**
     * Convert a PlayerVcurrencyBalance to a JSON which can be sent to the server.
     */
    private convert(playerVcurrencyBalance: PlayerVcurrencyBalance): PlayerVcurrencyBalance {
        const copy: PlayerVcurrencyBalance = Object.assign({}, playerVcurrencyBalance);
        return copy;
    }
}
