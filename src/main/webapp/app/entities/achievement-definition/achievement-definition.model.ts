import { BaseEntity } from './../../shared';

export const enum AchievementType {
    'XP_BASE',
    'LEVEL_BASE',
    'EVENT_BASE',
    'STATS_BASE',
    'TOURNAMENT_BASE'
}

export class AchievementDefinition implements BaseEntity {
    constructor(
        public id?: number,
        public avIconUrl?: string,
        public avCode?: string,
        public avNumber?: number,
        public avTitle?: string,
        public avType?: AchievementType,
        public repeteable?: boolean,
        public hidden?: boolean,
        public evCode?: string,
        public evField?: string,
        public stCode?: string,
        public lvCode?: string,
        public tnCode?: string,
        public goalMinValue?: number,
        public goalMaxValue?: number,
        public goalValue?: number,
        public rewardMessage?: string,
        public rewardXp?: number,
        public rewardCurrencyCode?: string,
        public rewardCurrencyValue?: string,
        public rewardCatalogCode?: string,
        public gameId?: number,
        public goalLevelId?: number,
        public goalEventId?: number,
        public goalStatisticId?: number,
        public rewardCatalogItemId?: number,
        public goalTournamentId?: number,
        public feeCurrencyId?: number,
    ) {
        this.repeteable = false;
        this.hidden = false;
    }
}
