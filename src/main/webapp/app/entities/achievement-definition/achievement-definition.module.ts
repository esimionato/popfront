import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PopSharedModule } from '../../shared';
import {
    AchievementDefinitionService,
    AchievementDefinitionPopupService,
    AchievementDefinitionComponent,
    AchievementDefinitionDetailComponent,
    AchievementDefinitionDialogComponent,
    AchievementDefinitionPopupComponent,
    AchievementDefinitionDeletePopupComponent,
    AchievementDefinitionDeleteDialogComponent,
    achievementDefinitionRoute,
    achievementDefinitionPopupRoute,
} from './';

const ENTITY_STATES = [
    ...achievementDefinitionRoute,
    ...achievementDefinitionPopupRoute,
];

@NgModule({
    imports: [
        PopSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AchievementDefinitionComponent,
        AchievementDefinitionDetailComponent,
        AchievementDefinitionDialogComponent,
        AchievementDefinitionDeleteDialogComponent,
        AchievementDefinitionPopupComponent,
        AchievementDefinitionDeletePopupComponent,
    ],
    entryComponents: [
        AchievementDefinitionComponent,
        AchievementDefinitionDialogComponent,
        AchievementDefinitionPopupComponent,
        AchievementDefinitionDeleteDialogComponent,
        AchievementDefinitionDeletePopupComponent,
    ],
    providers: [
        AchievementDefinitionService,
        AchievementDefinitionPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PopAchievementDefinitionModule {}
