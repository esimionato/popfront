import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AchievementDefinition } from './achievement-definition.model';
import { AchievementDefinitionPopupService } from './achievement-definition-popup.service';
import { AchievementDefinitionService } from './achievement-definition.service';

@Component({
    selector: 'jhi-achievement-definition-delete-dialog',
    templateUrl: './achievement-definition-delete-dialog.component.html'
})
export class AchievementDefinitionDeleteDialogComponent {

    achievementDefinition: AchievementDefinition;

    constructor(
        private achievementDefinitionService: AchievementDefinitionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.achievementDefinitionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'achievementDefinitionListModification',
                content: 'Deleted an achievementDefinition'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-achievement-definition-delete-popup',
    template: ''
})
export class AchievementDefinitionDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private achievementDefinitionPopupService: AchievementDefinitionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.achievementDefinitionPopupService
                .open(AchievementDefinitionDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
