import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CatalogItem } from './catalog-item.model';
import { CatalogItemPopupService } from './catalog-item-popup.service';
import { CatalogItemService } from './catalog-item.service';
import { Game, GameService } from '../game';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-catalog-item-dialog',
    templateUrl: './catalog-item-dialog.component.html'
})
export class CatalogItemDialogComponent implements OnInit {

    catalogItem: CatalogItem;
    isSaving: boolean;

    games: Game[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private catalogItemService: CatalogItemService,
        private gameService: GameService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.gameService.query()
            .subscribe((res: ResponseWrapper) => { this.games = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.catalogItem.id !== undefined) {
            this.subscribeToSaveResponse(
                this.catalogItemService.update(this.catalogItem));
        } else {
            this.subscribeToSaveResponse(
                this.catalogItemService.create(this.catalogItem));
        }
    }

    private subscribeToSaveResponse(result: Observable<CatalogItem>) {
        result.subscribe((res: CatalogItem) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: CatalogItem) {
        this.eventManager.broadcast({ name: 'catalogItemListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackGameById(index: number, item: Game) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-catalog-item-popup',
    template: ''
})
export class CatalogItemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private catalogItemPopupService: CatalogItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.catalogItemPopupService
                    .open(CatalogItemDialogComponent as Component, params['id']);
            } else {
                this.catalogItemPopupService
                    .open(CatalogItemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
