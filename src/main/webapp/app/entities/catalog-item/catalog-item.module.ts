import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PopSharedModule } from '../../shared';
import {
    CatalogItemService,
    CatalogItemPopupService,
    CatalogItemComponent,
    CatalogItemDetailComponent,
    CatalogItemDialogComponent,
    CatalogItemPopupComponent,
    CatalogItemDeletePopupComponent,
    CatalogItemDeleteDialogComponent,
    catalogItemRoute,
    catalogItemPopupRoute,
} from './';

const ENTITY_STATES = [
    ...catalogItemRoute,
    ...catalogItemPopupRoute,
];

@NgModule({
    imports: [
        PopSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CatalogItemComponent,
        CatalogItemDetailComponent,
        CatalogItemDialogComponent,
        CatalogItemDeleteDialogComponent,
        CatalogItemPopupComponent,
        CatalogItemDeletePopupComponent,
    ],
    entryComponents: [
        CatalogItemComponent,
        CatalogItemDialogComponent,
        CatalogItemPopupComponent,
        CatalogItemDeleteDialogComponent,
        CatalogItemDeletePopupComponent,
    ],
    providers: [
        CatalogItemService,
        CatalogItemPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PopCatalogItemModule {}
