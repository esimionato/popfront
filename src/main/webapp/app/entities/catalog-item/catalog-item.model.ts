import { BaseEntity } from './../../shared';

export const enum CatogItemStatus {
    'ACTIVE',
    'INACTIVE'
}

export const enum CatalogItemClass {
    'KEY',
    'USABLE_IN_COMBAT',
    'CLASS_TOKEN'
}

export const enum ConsumableType {
    'DURABLE',
    'CONSUMABLE_BY_COUNT',
    'CONSUMABLE_BY_TIME'
}

export class CatalogItem implements BaseEntity {
    constructor(
        public id?: number,
        public itemCode?: string,
        public displayName?: string,
        public description?: string,
        public options?: string,
        public metafields?: string,
        public itemStatus?: CatogItemStatus,
        public variants?: string,
        public customData?: string,
        public itemImageUrl?: string,
        public itemClass?: CatalogItemClass,
        public tags?: string,
        public consumable?: ConsumableType,
        public consumableCount?: number,
        public consumableTime?: number,
        public Stackable?: boolean,
        public Tradable?: boolean,
        public Limited?: boolean,
        public LimitedCount?: number,
        public priceAmount?: number,
        public gameId?: number,
    ) {
        this.Stackable = false;
        this.Tradable = false;
        this.Limited = false;
    }
}
