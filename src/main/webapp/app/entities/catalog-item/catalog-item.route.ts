import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CatalogItemComponent } from './catalog-item.component';
import { CatalogItemDetailComponent } from './catalog-item-detail.component';
import { CatalogItemPopupComponent } from './catalog-item-dialog.component';
import { CatalogItemDeletePopupComponent } from './catalog-item-delete-dialog.component';

export const catalogItemRoute: Routes = [
    {
        path: 'catalog-item',
        component: CatalogItemComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.catalogItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'catalog-item/:id',
        component: CatalogItemDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.catalogItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const catalogItemPopupRoute: Routes = [
    {
        path: 'catalog-item-new',
        component: CatalogItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.catalogItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'catalog-item/:id/edit',
        component: CatalogItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.catalogItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'catalog-item/:id/delete',
        component: CatalogItemDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.catalogItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
