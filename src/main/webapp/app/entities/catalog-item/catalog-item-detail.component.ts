import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { CatalogItem } from './catalog-item.model';
import { CatalogItemService } from './catalog-item.service';

@Component({
    selector: 'jhi-catalog-item-detail',
    templateUrl: './catalog-item-detail.component.html'
})
export class CatalogItemDetailComponent implements OnInit, OnDestroy {

    catalogItem: CatalogItem;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private catalogItemService: CatalogItemService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCatalogItems();
    }

    load(id) {
        this.catalogItemService.find(id).subscribe((catalogItem) => {
            this.catalogItem = catalogItem;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCatalogItems() {
        this.eventSubscriber = this.eventManager.subscribe(
            'catalogItemListModification',
            (response) => this.load(this.catalogItem.id)
        );
    }
}
