/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PopTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { AchievementDefinitionDetailComponent } from '../../../../../../main/webapp/app/entities/achievement-definition/achievement-definition-detail.component';
import { AchievementDefinitionService } from '../../../../../../main/webapp/app/entities/achievement-definition/achievement-definition.service';
import { AchievementDefinition } from '../../../../../../main/webapp/app/entities/achievement-definition/achievement-definition.model';

describe('Component Tests', () => {

    describe('AchievementDefinition Management Detail Component', () => {
        let comp: AchievementDefinitionDetailComponent;
        let fixture: ComponentFixture<AchievementDefinitionDetailComponent>;
        let service: AchievementDefinitionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PopTestModule],
                declarations: [AchievementDefinitionDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    AchievementDefinitionService,
                    JhiEventManager
                ]
            }).overrideTemplate(AchievementDefinitionDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AchievementDefinitionDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AchievementDefinitionService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new AchievementDefinition(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.achievementDefinition).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
