/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PopTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { XpLevelDefinitionDetailComponent } from '../../../../../../main/webapp/app/entities/xp-level-definition/xp-level-definition-detail.component';
import { XpLevelDefinitionService } from '../../../../../../main/webapp/app/entities/xp-level-definition/xp-level-definition.service';
import { XpLevelDefinition } from '../../../../../../main/webapp/app/entities/xp-level-definition/xp-level-definition.model';

describe('Component Tests', () => {

    describe('XpLevelDefinition Management Detail Component', () => {
        let comp: XpLevelDefinitionDetailComponent;
        let fixture: ComponentFixture<XpLevelDefinitionDetailComponent>;
        let service: XpLevelDefinitionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PopTestModule],
                declarations: [XpLevelDefinitionDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    XpLevelDefinitionService,
                    JhiEventManager
                ]
            }).overrideTemplate(XpLevelDefinitionDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(XpLevelDefinitionDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(XpLevelDefinitionService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new XpLevelDefinition(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.xpLevelDefinition).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
