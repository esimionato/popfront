/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PopTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { DataDefinitionDetailComponent } from '../../../../../../main/webapp/app/entities/data-definition/data-definition-detail.component';
import { DataDefinitionService } from '../../../../../../main/webapp/app/entities/data-definition/data-definition.service';
import { DataDefinition } from '../../../../../../main/webapp/app/entities/data-definition/data-definition.model';

describe('Component Tests', () => {

    describe('DataDefinition Management Detail Component', () => {
        let comp: DataDefinitionDetailComponent;
        let fixture: ComponentFixture<DataDefinitionDetailComponent>;
        let service: DataDefinitionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PopTestModule],
                declarations: [DataDefinitionDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    DataDefinitionService,
                    JhiEventManager
                ]
            }).overrideTemplate(DataDefinitionDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DataDefinitionDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DataDefinitionService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new DataDefinition(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.dataDefinition).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
