/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PopTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { EventDefinitionDetailComponent } from '../../../../../../main/webapp/app/entities/event-definition/event-definition-detail.component';
import { EventDefinitionService } from '../../../../../../main/webapp/app/entities/event-definition/event-definition.service';
import { EventDefinition } from '../../../../../../main/webapp/app/entities/event-definition/event-definition.model';

describe('Component Tests', () => {

    describe('EventDefinition Management Detail Component', () => {
        let comp: EventDefinitionDetailComponent;
        let fixture: ComponentFixture<EventDefinitionDetailComponent>;
        let service: EventDefinitionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PopTestModule],
                declarations: [EventDefinitionDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    EventDefinitionService,
                    JhiEventManager
                ]
            }).overrideTemplate(EventDefinitionDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EventDefinitionDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EventDefinitionService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new EventDefinition(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.eventDefinition).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
