/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PopTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { CatalogItemDetailComponent } from '../../../../../../main/webapp/app/entities/catalog-item/catalog-item-detail.component';
import { CatalogItemService } from '../../../../../../main/webapp/app/entities/catalog-item/catalog-item.service';
import { CatalogItem } from '../../../../../../main/webapp/app/entities/catalog-item/catalog-item.model';

describe('Component Tests', () => {

    describe('CatalogItem Management Detail Component', () => {
        let comp: CatalogItemDetailComponent;
        let fixture: ComponentFixture<CatalogItemDetailComponent>;
        let service: CatalogItemService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PopTestModule],
                declarations: [CatalogItemDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    CatalogItemService,
                    JhiEventManager
                ]
            }).overrideTemplate(CatalogItemDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CatalogItemDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CatalogItemService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new CatalogItem(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.catalogItem).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
