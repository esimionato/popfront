/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PopTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { PlayerVcurrencyBalanceDetailComponent } from '../../../../../../main/webapp/app/entities/player-vcurrency-balance/player-vcurrency-balance-detail.component';
import { PlayerVcurrencyBalanceService } from '../../../../../../main/webapp/app/entities/player-vcurrency-balance/player-vcurrency-balance.service';
import { PlayerVcurrencyBalance } from '../../../../../../main/webapp/app/entities/player-vcurrency-balance/player-vcurrency-balance.model';

describe('Component Tests', () => {

    describe('PlayerVcurrencyBalance Management Detail Component', () => {
        let comp: PlayerVcurrencyBalanceDetailComponent;
        let fixture: ComponentFixture<PlayerVcurrencyBalanceDetailComponent>;
        let service: PlayerVcurrencyBalanceService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PopTestModule],
                declarations: [PlayerVcurrencyBalanceDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    PlayerVcurrencyBalanceService,
                    JhiEventManager
                ]
            }).overrideTemplate(PlayerVcurrencyBalanceDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PlayerVcurrencyBalanceDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PlayerVcurrencyBalanceService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new PlayerVcurrencyBalance(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.playerVcurrencyBalance).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
